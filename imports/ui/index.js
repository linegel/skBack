import { Template } from 'meteor/templating';
import { PublicProfile } from '../api/users/publicProfiles.js';
import './index.html';

Template.Test.onCreated(function bodyOnCreated() {
  Meteor.subscribe('PublicProfile');
});

Template.Test.helpers({
  PublicProfile() {
    return PublicProfile.find({}, { sort: { createdAt: -1 } });
  },
});