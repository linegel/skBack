import {_} from 'meteor/underscore'
import {check} from 'meteor/check'

class TestCollection extends Mongo.Collection
  insert: (doc, callback) ->
    ourDoc = doc
    ourDoc.createdAt = ourDoc.createdAt or new Date()
    result = super ourDoc, callback
    incompleteCountDenormalizer.afterInsertTodo ourDoc
    result

  update: (selector, modifier) ->
    result = super selector, modifier
    incompleteCountDenormalizer.afterUpdateTodo selector, modifier
    result


  remove: (selector) ->
    test = @find(selector).fetch()
    result = super selector
    incompleteCountDenormalizer.afterRemoveTodos test
    result

TestCollection.deny
  insert: -> yes
  update: -> yes
  remove: -> yes