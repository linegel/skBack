import { Meteor } from 'meteor/meteor';
import { Mongo } from 'meteor/mongo';
import { check } from 'meteor/check';

export const PublicProfile = new Mongo.Collection('PublicProfile');

if (Meteor.isServer) {
  Meteor.publish('PublicProfile', function() {
  return PublicProfile.find({}, { sort: { createdAt: -1 } });
})}

Meteor.methods({
  'PublicProfile.insert'(text) {
    check(text, String);

    // Make sure the user is logged in before inserting a profile
    // if (! this.userId) {
    //   throw new Meteor.Error('not-authorized');
    // }

    PublicProfile.insert({
      text,
      createdAt: new Date(),
      owner: this.userId,
      username: Meteor.users.findOne(this.userId).username,
    });
  },
  'PublicProfile.remove'(PublicProfileId) {
    check(PublicProfileId, String);

    const profile = PublicProfile.findOne(PublicProfileId);
    if (profile.private && profile.owner !== this.userId) {
      // If the profile is private, make sure only the owner can delete it
      throw new Meteor.Error('not-authorized');
    }

    PublicProfile.remove(PublicProfileId);
  },
  'PublicProfile.setChecked'(PublicProfileId, setChecked) {
    check(PublicProfileId, String);
    check(setChecked, Boolean);

    const profile = PublicProfile.findOne(PublicProfileId);
    if (profile.private && profile.owner !== this.userId) {
      // If the profile is private, make sure only the owner can check it off
      throw new Meteor.Error('not-authorized');
    }

    PublicProfile.update(PublicProfileId, { $set: { checked: setChecked } });
  },
  'PublicProfile.setPrivate'(PublicProfileId, setToPrivate) {
    check(PublicProfileId, String);
    check(setToPrivate, Boolean);

    const profile = PublicProfile.findOne(PublicProfileId);

    // Make sure only the profile owner can make a profile private
    if (profile.owner !== this.userId) {
      throw new Meteor.Error('not-authorized');
    }

    PublicProfile.update(PublicProfileId, { $set: { private: setToPrivate } });
  },
});