module.exports = {
  servers: {
    one: {
      host: '159.100.251.65',
      username: 'ubuntu',
      pem: '../../exo'
    }
  },

  app: {
    name: 'sharekey',
    path: '../',
    servers:{
      one:{}
    },
    buildOptions: {
      serverOnly: true,
    },

    env: {
      ROOT_URL: 'http://localhost:3000/',
      MONGO_URL: 'mongodb://localhost/meteor',
    },

    docker: {
      image: 'abernix/meteord:base',
    },
    enableUploadProgressBar: true
  },

  mongo: {
    version: '3.4.1 ',
    servers:{
      one:{}
    }
  }
};
